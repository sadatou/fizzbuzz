package fizzbuzz;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FizzBuzzTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void 引数に9を与えたらFizzを返す() {
		String result1 = FizzBuzz.checkFizzBuzz(9);

		assertEquals(result1, ("Fizz"));
	}

	@Test
	void 引数に20を与えたらBuzzを返す() {
		String result2 = FizzBuzz.checkFizzBuzz(20);

		assertEquals(result2, ("Buzz"));
	}

	@Test
	void 引数に45を与えたらFizzBuzzを返す() {
		String result3 = FizzBuzz.checkFizzBuzz(45);

		assertEquals(result3, ("FizzBuzz"));
	}

	@Test
	void 引数に44を与えたら44を返す() {
		String result4 = FizzBuzz.checkFizzBuzz(44);

		assertEquals(result4, ("44"));
	}

	@Test
	void 引数に46を与えたら46を返す() {
		String result5 = FizzBuzz.checkFizzBuzz(46);

		assertEquals(result5, ("46"));
	}
}
